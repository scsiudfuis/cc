import requests
import allure


@allure.step("新增宠物")
def test_add_pet():
    url = 'https://petstore.swagger.io/v2/pet'
    data = {
        "id": 1,
        "category": {
            "id": 1,
            "name": "猫"
        },
        "name": "Tom",
        "photoUrls": [],
        "tags": [
            {
                "id": 1,
                "name": "可爱"
            }
        ],
        "status": "available"
    }
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, json=data, headers=headers)
    assert response.status_code == 200


@allure.step("查询宠物")
def test_find_pet():
    url = 'https://petstore.swagger.io/v2/pet/findByStatus'
    params = {'status': 'available'}
    response = requests.get(url, params=params)
    assert response.status_code == 200
