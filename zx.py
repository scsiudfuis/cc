import pytest
def add(a,b):
    return a+b


def setup_function():
    print("开始计算")


def teardown_function():
    print("结束计算")


def teardown():
    print("结束测试")


@pytest.mark.hebeu
def test_add_integers():
    assert add(1, 4) == 5
    assert add(0, 0) == 0
    assert add(-1, 2) == 1
    assert add(-98, 99) == 1


@pytest.mark.hebeu
def test_add_floats():
    assert add(2.6, 3.4) == 6.0
    assert add(0.0, 0.0) == 0.0
    assert add(-1.0, 2.5) == 1.5
    assert pytest.approx(add(-98.9, 99.0) ,0.001)== 0.1


@pytest.mark.hebeu
def test_add_mixed():
    assert add(3, 2.5) == 5.5
    assert add(-1, 1.5) == 0.5
    assert add(0, 3.24) == 3.24
    assert pytest.approx(add(99, -98.99), 0.001) == 0.01



