
from selenium import webdriver
from selenium.webdriver.common.by import By

from home_page import HomePage


class TestCeshiren:

    def setup_class(self):
        # STEP1: 打开浏览器
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(3)
        self.driver.get("https://ceshiren.com")

    def test_search_old(self):
        word = "测试"

        # 3：点击搜索按钮
        self.driver.find_element(By.ID, "search-button").click()

        # 输入关键词：“测试”
        self.driver.find_element(By.ID, "search-term").send_keys(word)

        # 点击放大镜条目
        self.driver.find_element(By.CLASS_NAME, "keyword").click()

        # 展示查询结果列表
        elements = self.driver.find_elements(By.CLASS_NAME, "search-highlight")

        result_text_list = [ele.text for ele in elements]

        assert word in result_text_list

    def test_search_long_text(self):
        word = "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试"
        results = HomePage(self.driver).search(word)
        assert word not in results

