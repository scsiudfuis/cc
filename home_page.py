import time

from selenium.webdriver.common.by import By


class HomePage:

    BTN_SEARCH = By.ID, "search-button"
    INP_SEARCH = By.ID, "search-term"
    ITEM_SEARCH = By.CLASS_NAME, "keyword"
    LIST_RESULT = By.CLASS_NAME, "search-highlight"

    def __init__(self, base_driver):
        self.driver = base_driver


    def search(self, word):

        # 2：访问地址
        self.driver.get("https://ceshiren.com")

        # 3：点击搜索按钮
        self.driver.find_element(*self.BTN_SEARCH).click()

        # 输入关键词：“测试”
        self.driver.find_element(*self.INP_SEARCH).send_keys(word)

        # 点击放大镜条目
        self.driver.find_element(*self.ITEM_SEARCH).click()

        # 展示查询结果列表
        elements = self.driver.find_elements(*self.LIST_RESULT)

        result_text_list = [ele.text for ele in elements]

        return result_text_list
        # return RigisterPage(self.driver)

    def xx(self):
        pass